import { Component, OnInit, ViewChild } from "@angular/core";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
} from "@angular/material/dialog";
import { DialogBoxComponent } from "../dialog-box/dialog-box.component";
import { TranslateService } from "@ngx-translate/core";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-approver",
  templateUrl: "./approver.component.html",
  styleUrls: ["./approver.component.scss"]
})
export class ApproverComponent implements OnInit {
  change: boolean = false;

  columnDefs;
  gridApi;
  gridColumnApi;
  rowData;

  constructor(
    public dialog: MatDialog,
    private translateService: TranslateService,
    private http: HttpClient
  ) {
    translateService.setDefaultLang("ja");
    this.columnDefs = [
      {
        headerName: "Txn Number",
        field: "txnNumber",
        sortable: true,
        filter: true
      },
      { headerName: "Date", field: "date", sortable: true, filter: true },
      {
        headerName: "Form Type",
        field: "formType",
        sortable: true,
        filter: true
      },
      {
        headerName: "Requestor Name",
        field: "requestorName",
        sortable: true,
        filter: true
      },
      {
        headerName: "Target User Name",
        field: "targetUserName",
        sortable: true,
        filter: true
      },
      {
        headerName: "Bulk Approval",
        field: "bulkApproval",
        checkboxSelection: true
      }
    ];
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: "260px"
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("The dialog was closed");
    });
  }
  ngOnInit() {}

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.rowData = [
      {
        txnNumber: "4686",
        date: "12-12-1996",
        formType: "System Access Request",
        requestorName: "Gary",
        targetUserName: "Gary",
        bulkApproval: ""
      },
      {
        txnNumber: "4687",
        date: "1-1-2000",
        formType: "System Access Request",
        requestorName: "jane",
        targetUserName: "Gary",
        bulkApproval: ""
      },
      {
        txnNumber: "4689",
        date: "18-7-2016",
        formType: "System Access Request",
        requestorName: "derry",
        targetUserName: "Gary",
        bulkApproval: ""
      }
    ];
  }

  switchLanguage() {
    this.change = !this.change;
    if (this.change) {
      this.translateService.use("en");
    } else {
      this.translateService.use("ja");
    }
  }
}
